﻿using System.Xml.Linq;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace NFox.Runtime.Com.Reflection
{
    /// <summary>
    /// 函数参数信息
    /// </summary>
    public class Parameter : MemberBase
    {
        internal Parameter(XElement xe)
            : base(xe)
        { }

        public Parameter(int id, string name, ComTypes.ITypeInfo info, ComTypes.ELEMDESC desc)
            : base(id, name)
        {
            Element = new Element(info, desc);
        }

        protected override string Category
        {
            get { return "Parameter"; }
        }
    }
}